import * as ErrorTypes from './Error.types';

const initialState = {
  error: null
};

export const errorReducer = (state = initialState, action) => {
  switch (action.type) {
    case ErrorTypes.ADD_ERROR:
      return { ...state, error: action.payload };
    case ErrorTypes.REMOVE_ERROR:
      return { ...state, error: null };
    default:
      return state;
  }
};
