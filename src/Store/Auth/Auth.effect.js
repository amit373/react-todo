import HttpService from './../../Services/HttpService';
import { addError } from './../Error/Error.action';
import * as authActions from './Auth.action';

export const loginUser = data => {
  return async dispatch => {
    try {
      const { ...user } = await HttpService.post(`auth/login`, data);
      localStorage.setItem('loginRsp', JSON.stringify(user));
      dispatch(authActions.setCurrentUser(user));
      return user;
    } catch (err) {
      const { error } = err.response.data;
      dispatch(addError(error));
    }
  };
};

export const logoutUser = () => dispatch => {
  localStorage.clear();
  dispatch(authActions.logoutUser());
};
