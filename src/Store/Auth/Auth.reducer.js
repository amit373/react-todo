import * as AuthTypes from './Auth.types';

const initialState = {
  error: null,
  user: {},
  loading: true,
  isAuthenticated: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AuthTypes.LOGIN_USER:
      return {
        ...state,
        user: action.payload,
        loading: false,
        isAuthenticated: true
      };
    case AuthTypes.LOGOUT_USER:
      return {
        ...state,
        user: {},
        loading: false,
        isAuthenticated: false
      };
    default:
      return state;
  }
};
