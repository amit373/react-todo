import { LOGIN_USER, LOGOUT_USER } from './Auth.types';

export const setCurrentUser = user => ({
  type: LOGIN_USER,
  payload: user
});

export const logoutUser = () => ({
  type: LOGOUT_USER,
  payload: {}
});
