import HttpService from './../../Services/HttpService';
import { addError } from './../Error/Error.action';
import * as todoActions from './Todo.action';

export const loadTodos = () => {
  return async dispatch => {
    try {
      const todos = await HttpService.get(`tasks/`);
      dispatch(todoActions.loadTodos(todos));
      return todos;
    } catch (err) {
      dispatch(addError(err.response.data));
    }
  };
};

export const loadSingleTodo = id => {
  return async dispatch => {
    try {
      const todo = await HttpService.get(`tasks/${id}`);
      dispatch(todoActions.loadSingleTodo(todo));
      return todo;
    } catch (err) {
      dispatch(addError(err.response.data));
    }
  };
};

export const addTodo = data => {
  return async dispatch => {
    try {
      const todo = await HttpService.post(`tasks/`, data);
      dispatch(todoActions.addTodo(todo));
      return todo;
    } catch (err) {
      dispatch(addError(err.response.data));
    }
  };
};

export const editTodo = (data, id) => {
  return async dispatch => {
    try {
      const todo = await HttpService.put(`tasks/${id}`, data);
      dispatch(todoActions.editTodo(todo, id));
      return todo;
    } catch (err) {
      dispatch(addError(err.response.data));
    }
  };
};

export const deleteTodo = id => {
  return async dispatch => {
    try {
      const todo = await HttpService.deleteData(`tasks/${id}`);
      const response = { ...todo, id };
      dispatch(todoActions.deleteTodo(response));
      return response;
    } catch (err) {
      dispatch(addError(err.response.data));
    }
  };
};
