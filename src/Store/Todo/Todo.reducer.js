import * as todoTypes from './Todo.types';

const initialState = {
  error: null,
  todos: {},
  loading: true
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case todoTypes.LOAD_TODOS:
      return {
        ...state,
        todos: action.payload,
        loading: false
      };
    case todoTypes.LOAD_SINGLE_TODO:
      return {
        ...state,
        todo: action.payload,
        loading: false
      };
    case todoTypes.ADD_TODO:
      return {
        ...state,
        todo: action.payload.data,
        loading: false
      };
    case todoTypes.EDIT_TODO:
      return {
        ...state,
        todos: action.payload,
        loading: false
      };
    case todoTypes.DELETE_TODO:
      return {
        ...state,
        deletedTodo: action.payload,
        loading: false
      };
    default:
      return state;
  }
};
