export const LOAD_TODOS = '[TODO] Load todos';
export const LOAD_SINGLE_TODO = '[TODO] Load Todo';
export const ADD_TODO = '[TODO] Add Todo';
export const EDIT_TODO = '[TODO] Edit Todo';
export const DELETE_TODO = '[TODO] Delete Todo';
