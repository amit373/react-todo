import {
  LOAD_TODOS,
  LOAD_SINGLE_TODO,
  ADD_TODO,
  EDIT_TODO,
  DELETE_TODO
} from './Todo.types';

export const loadTodos = data => ({
  type: LOAD_TODOS,
  payload: data.data
});

export const loadSingleTodo = data => ({
  type: LOAD_SINGLE_TODO,
  payload: data
});

export const addTodo = data => ({
  type: ADD_TODO,
  payload: data
});

export const editTodo = data => ({
  type: EDIT_TODO,
  payload: data
});

export const deleteTodo = data => ({
  type: DELETE_TODO,
  payload: data
});
