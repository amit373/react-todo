import { combineReducers } from 'redux';
import { authReducer } from './Auth/Auth.reducer';
import { errorReducer } from './Error/Error.reducer';
import { todoReducer } from './Todo/Todo.reducer';

export default combineReducers({
  errorReducer,
  authReducer,
  todoReducer
});
