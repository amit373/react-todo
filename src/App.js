import React, { Fragment, Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import './App.css';
import Login from './Components/Login/Login';
import Navbar from './Components/Containers/Navbar';
import Footer from './Components/Containers/Footer';
import TodoDetail from './Components/Todo/TodoDetail';
import Todos from './Components/Todo/Todos';

import ProtectedRoutes from './Components/Containers/ProtectedRoutes';
import store from './Store/store';
import { setCurrentUser } from './Store/Auth/Auth.action';
import { logoutUser } from './Store/Auth/Auth.effect';
import { connect } from 'react-redux';

if (localStorage.loginRsp) {
  const userRsp = JSON.parse(localStorage.getItem('loginRsp'));
  if (userRsp) {
    store.dispatch(setCurrentUser(userRsp));
    const currentTime = Date.now() / 1000;
    if (userRsp.exp < currentTime) {
      store.dispatch(logoutUser());
      window.location.href = '/login';
    }
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }

  componentDidMount() {
    const userRsp = JSON.parse(localStorage.getItem('loginRsp'));
    if (userRsp) this.props.history.push('/todos');
  }

  onLogoutHandler = () => {
    this.props.logout();
    this.props.history.push('/login');
  };

  render() {
    return (
      <Fragment>
        <Navbar
          onLogout={this.onLogoutHandler}
          userData={this.props.userData}
        />
        <Switch>
          <Route exact path='/' component={Login} />
          <Route exact path='/login' component={Login} />
          <ProtectedRoutes exact path='/todos' component={Todos} />
          <ProtectedRoutes exact path='/todos/:id' component={TodoDetail} />
        </Switch>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { ...userData } = state.authReducer;
  return { userData };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
