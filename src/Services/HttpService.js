import axios from 'axios';

const environment = 'http://localhost:5000/api/v1';

export const setToken = token => {
  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
};

export const setHeaders = () => {
  const loginRsp = JSON.parse(localStorage.getItem('loginRsp'));
  const headers = loginRsp ? setToken(loginRsp.token) : null;
  return headers;
};

export const call = async (method, path, data) => {
  const response = await axios[method](`${environment}/${path}`, data);
  return response.data;
};

export const get = async endpoint => {
  const response = await axios.get(`${environment}/${endpoint}`, setHeaders());
  return response.data;
};

export const post = async (endpoint, data) => {
  const response = await axios.post(
    `${environment}/${endpoint}`,
    data,
    setHeaders()
  );
  return response.data;
};

export const put = async (endpoint, data) => {
  const response = await axios.put(
    `${environment}/${endpoint}`,
    data,
    setHeaders()
  );
  return response.data;
};

export const patch = async (endpoint, data) => {
  const response = await axios.patch(
    `${environment}/${endpoint}`,
    data,
    setHeaders()
  );
  return response.data;
};

export const deleteData = async endpoint => {
  const response = await axios.delete(
    `${environment}/${endpoint}`,
    setHeaders()
  );
  return response.data;
};

export const uploadImage = async (endpoint, data) => {
  const response = await axios.post(
    `${environment}/${endpoint}`,
    data,
    setHeaders()
  );
  return response.data;
};

export default {
  setToken,
  call,
  get,
  post,
  put,
  patch,
  deleteData,
  uploadImage
};
