import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginUser } from './../../Store/Auth/Auth.effect';

class Login extends Component {
  state = {
    email: '',
    password: '',
    rememberMe: false
  };

  setToken = user => {
    localStorage.setItem('loginRsp', JSON.stringify(user));
  };

  handleChange = e => {
    const { rememberMe } = this.state;
    this.setState({ [e.target.name]: e.target.value, rememberMe: !rememberMe });
  };

  onLoginHandler = e => {
    e.preventDefault();
    const { email, password } = this.state;
    const dataObj = {
      email,
      password
    };
    this.props.loginUser(dataObj).then(data => {
      if (data) this.props.history.push('/todos');
    });
  };

  render() {
    const { email, password, rememberMe } = this.state;
    return (
      <div>
        <div className='container'>
          <br />
          <h3 className='text-center'>Login</h3>
          <div className='row'>
            <div className='offset-md-3 col-md-5'>
              <form onSubmit={this.onLoginHandler}>
                <div className='form-group'>
                  <label htmlFor='email'>Email address:</label>
                  <input
                    type='email'
                    className='form-control'
                    placeholder='Enter email'
                    id='email'
                    name='email'
                    value={email}
                    onChange={this.handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='pwd'>Password:</label>
                  <input
                    type='password'
                    className='form-control'
                    placeholder='Enter password'
                    id='pwd'
                    name='password'
                    value={password}
                    onChange={this.handleChange}
                  />
                </div>
                <div className='form-group form-check'>
                  <label className='form-check-label'>
                    <input
                      className='form-check-input'
                      name='rememberMe'
                      value={rememberMe}
                      onChange={this.handleChange}
                      type='checkbox'
                    />{' '}
                    Remember me
                  </label>
                </div>
                <button
                  disabled={!rememberMe}
                  type='submit'
                  className='btn btn-primary'
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  loginUser: data => dispatch(loginUser(data))
});

export default connect(null, mapDispatchToProps)(Login);
