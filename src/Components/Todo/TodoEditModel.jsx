import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const TodoEditModal = props => {
  const {
    toggleModel,
    todo,
    onChange,
    onClose,
    onEdit,
    onAddTodo,
    isLoading,
    onChangeRadio
  } = props;
  return (
    <>
      <Modal show={toggleModel}>
        <Modal.Header>
          <Modal.Title>{todo._id ? 'Edit Todo' : 'Add Todo'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='input-group mb-3'>
              <div className='input-group-prepend'>
                <span className='input-group-text'>Title</span>
              </div>
              <input
                type='text'
                className='form-control'
                placeholder='Title'
                name='title'
                onChange={onChange}
                value={todo.title}
              />
            </div>
            <div className='input-group mb-3'>
              <div className='input-group-prepend'>
                <span className='input-group-text'>Description</span>
              </div>
              <input
                type='text'
                className='form-control'
                placeholder='Description'
                name='description'
                onChange={onChange}
                value={todo.description}
              />
            </div>
            <div>
              <div className='input-group-prepend'>
                <span className='input-group-text mr-3'>Status</span>
                <div className='form-check-inline'>
                  <label className='form-check-label'>
                    <input
                      type='radio'
                      className='form-check-input'
                      name='OPEN'
                      onChange={onChangeRadio}
                      checked={todo.status === 'OPEN'}
                      value='OPEN'
                    />
                    Open
                  </label>
                </div>
                <div className='form-check-inline'>
                  <label className='form-check-label'>
                    <input
                      type='radio'
                      className='form-check-input'
                      name='CLOSE'
                      onChange={onChangeRadio}
                      checked={todo.status === 'CLOSE'}
                      value='CLOSE'
                    />
                    Close
                  </label>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={onClose}>
            Close
          </Button>
          {isLoading ? (
            <Button disabled={true}>
              <i className='fa fa-spinner fa-spin text-black' />
            </Button>
          ) : (
            <Button
              disabled={todo.title === '' && todo.description === ''}
              variant='primary'
              onClick={todo._id ? () => onEdit(todo) : onAddTodo}
            >
              Save Changes
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default TodoEditModal;
