import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';
import { loadSingleTodo } from '../../Store/Todo/Todo.effect';

class TodoDetail extends Component {
  state = {
    todo: {}
  };
  async componentDidMount() {
    await this.getTodoByDetail();
  }

  getTodoByDetail = async () => {
    const { match } = this.props;
    const todo = await this.props.todoById(match.params.id);
    const { data } = todo;
    data.createdAt = moment(data.createdAt).format('MMM Do YYYY');
    this.setState({ todo: data });
  };

  render() {
    const { todo } = this.state;
    return (
      <Fragment>
        <div class='container mt-3'>
          <h3>Todo Detail</h3>
          <div class='media border p-3'>
            <div class='media-body'>
              <h4>
                {todo.title} <br />
                <small>
                  <i>Posted on {todo.createdAt}</i>
                </small>
              </h4>
              <p>
                Status:
                <span
                  className={
                    todo.status === 'OPEN' ? 'text-success' : 'text-danger'
                  }
                >
                  {' '}
                  {todo.status}
                </span>
              </p>
              <p>
                Description: <span>{todo.description}</span>
              </p>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

// const mapStateToProps = state => {
//   const { todos } = state.todoReducer;
//   return { todos };
// };

const mapDispatchToProps = dispatch => {
  return {
    todoById: id => dispatch(loadSingleTodo(id))
  };
};

export default connect(null, mapDispatchToProps)(TodoDetail);
