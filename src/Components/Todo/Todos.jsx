import React, { Component, Fragment } from 'react';
import _ from 'lodash';
import httpService from './../../Services/HttpService';
import Swal from 'sweetalert2';
import toastr from 'toastr';
import TodoEditModal from './TodoEditModel';
import { connect } from 'react-redux';
import {
  loadTodos,
  addTodo,
  editTodo,
  deleteTodo
} from '../../Store/Todo/Todo.effect';

class Todos extends Component {
  state = {
    todos: {},
    toggleModel: false,
    todo: {},
    todoDetail: {},
    _id: '',
    title: '',
    description: '',
    status: 'OPEN',
    loading: false
  };

  async componentDidMount() {
    await this.getTodos();
  }

  async getTodoById(id) {
    try {
      await httpService.get(`tasks/${id}`);
    } catch (error) {
      console.log(error);
    }
  }

  async getTodos() {
    const todos = await this.props.todos();
    this.setState({ todos: todos.data });
  }

  addTodoHandler = async () => {
    this.state.loading = true;
    const { title, description, status } = this.state;
    const dataObj = {
      title,
      description,
      status
    };
    try {
      const todoRsp = await this.props.addTodo(dataObj);
      this.setState({
        toggleModel: !this.state.toggleModel,
        todos: [todoRsp.data, ...this.state.todos],
        loading: !this.state.loading
      });
      Swal.fire('Added!', 'Todo has been Added.', 'success');
    } catch (error) {
      console.log(error);
    }
  };

  editTodoHandler = async todo => {
    try {
      await this.props.editTodo(todo, todo._id);
      const todos = await this.props.todos();
      Swal.fire('Updates!', 'Your file has been updated.', 'success');
      this.setState({
        toggleModel: !this.state.toggleModel,
        todos: todos.data
      });
    } catch (error) {
      console.log(error);
    }
  };

  deleteTodoHandler = todo => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async result => {
      if (result.value) {
        try {
          const res = await this.props.deleteTodo(todo._id);
          const todos = _.filter(this.state.todos, obj => obj._id !== res.id);
          this.setState({ todos });
          toastr.success('Deleted!', 'Your file has been deleted.');
          // Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
        } catch (error) {
          console.log(error);
        }
      }
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleChangeRadio = e => {
    this.setState({
      status: this.state.status === 'OPEN' ? 'CLOSE' : 'OPEN'
    });
  };

  toggleModel = selectedTodo => {
    this.setState({
      toggleModel: !this.state.toggleModel,
      title: selectedTodo ? selectedTodo.title : '',
      description: selectedTodo ? selectedTodo.description : '',
      _id: selectedTodo ? selectedTodo._id : '',
      status: selectedTodo ? selectedTodo.status : 'OPEN'
    });
  };

  goToTodoDetail = todo => {
    this.setState({ todoDetail: todo });
    this.props.history.push(`/todos/${todo._id}`);
  };

  render() {
    let todo = {
      _id: this.state._id,
      title: this.state.title,
      description: this.state.description,
      status: this.state.status
    };

    const iconStyles = {
      cursor: 'pointer'
    };
    return (
      <Fragment>
        <div className='container'>
          <div className='row'>
            <div className='col-md-12'>
              <br />
              <h2 className='text-center'>
                Todo List{' '}
                <span>
                  <button
                    className='btn btn-success pr-2'
                    onClick={() => this.toggleModel(null)}
                  >
                    {' '}
                    Add Todo
                  </button>
                </span>
              </h2>
              <table className='table table-hover'>
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Detail</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {_.map(this.state.todos, todo => (
                    <tr key={todo._id}>
                      <td>{todo.title}</td>
                      <td>{todo.description}</td>
                      <td
                        className={
                          todo.status === 'OPEN'
                            ? 'text-success'
                            : 'text-danger'
                        }
                      >
                        {todo.status}
                      </td>
                      <td onClick={() => this.goToTodoDetail(todo)}>
                        <i style={iconStyles} className='fas fa-info-circle' />
                      </td>
                      <td onClick={() => this.toggleModel(todo)}>
                        <i style={iconStyles} className='fa fa-edit' />
                      </td>
                      <td onClick={() => this.deleteTodoHandler(todo)}>
                        <i style={iconStyles} className='fa fa-trash' />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
          <TodoEditModal
            toggleModel={this.state.toggleModel}
            todo={todo}
            onChange={this.handleChange}
            onChangeRadio={this.handleChangeRadio}
            onClose={this.toggleModel}
            onEdit={this.editTodoHandler}
            onAddTodo={this.addTodoHandler}
            isLoading={this.state.loading}
          />
        </div>
      </Fragment>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  todos: () => dispatch(loadTodos()),
  addTodo: data => dispatch(addTodo(data)),
  editTodo: (data, id) => dispatch(editTodo(data, id)),
  deleteTodo: id => dispatch(deleteTodo(id))
});

export default connect(null, mapDispatchToProps)(Todos);
