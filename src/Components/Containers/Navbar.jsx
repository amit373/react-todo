import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
  render() {
    return (
      <div>
        <nav className='navbar navbar-expand-sm bg-dark navbar-dark'>
          <Link className='navbar-brand' to='/todos'>
            Todos
          </Link>
          <button
            className='navbar-toggler'
            type='button'
            data-toggle='collapse'
            data-target='#collapsibleNavbar'
          >
            <span className='navbar-toggler-icon'></span>
          </button>
          <div className='collapse navbar-collapse' id='collapsibleNavbar'>
            <ul className='navbar-nav'>
              {this.props.userData.isAuthenticated && this.props.userData.user && (
                <Fragment>
                  <li className='nav-item'>
                    <div className='nav-link'>
                      {this.props.userData.user.name}
                    </div>
                  </li>
                  <li className='nav-item' onClick={this.props.onLogout}>
                    <div className='nav-link'>Logout</div>
                  </li>
                </Fragment>
              )}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
